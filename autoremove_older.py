#!/usr/bin/env python3
# -*- coding: utf8 -*-

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#


import sys, os, re, argparse, requests, json, shutil, subprocess
from pprint import pprint

parser = argparse.ArgumentParser(description = 'Supprimer les anciens enregistrements de tvheadend 4.0 pour conserver une quantitée d\'espace disque libre donnée.')

parser.add_argument('--records-dir', required=True, help = 'dossier contenant les enregisrements')
parser.add_argument('--hts-user', default = 'admin', help = 'nom d\'utilisateur hts (admin par défaut)')
parser.add_argument('--hts-password', default = 'admin', help = 'mot de passe hts (admin par défaut)')
parser.add_argument('--disk-free', default = '5Gio', help = 'espace libre désiré en o, Kio,Mio, Gio ou Tio (5Gio par défaut)')
parser.add_argument('--exclude', help = 'id des enregistrements à exclure de l\'effacement (séparateur ",")')
parser.add_argument('--yes-all', action = 'store_true', help = 'suppression sans confirmation')

args = parser.parse_args()

def human_size(Kio) :
    if Kio > 2**30 :
        return "%.1fTio"%(float(Kio)/2**30)
    elif Kio > 2**20 :
        return "%.1fGio"%(float(Kio)/2**20)
    elif Kio > 2**10 :
        return "%.1fMio"%(float(Kio)/2**10)
    else :
        return "%.1fKio"%(float(Kio))

if args.exclude :
    exclude = args.exclude.split(',')
else :
    exclude = []

df_req = args.disk_free
try :
    if df_req.endswith('Tio') :
        df_req = float(df_req[:-3])*2**30
    elif df_req.endswith('Gio') :
        df_req = float(df_req[:-3])*2**20
    elif df_req.endswith('Mio') :
        df_req = float(df_req[:-3])*2**10
    elif df_req.endswith('Kio') :
        df_req = float(df_req[:-3])
    else :
        parser.error('--disk-free bad value')
except :
    exit(1)

r = requests.post(
    'http://localhost:9981/api/dvr/entry/grid_finished',
    auth=(args.hts_user, args.hts_password),
    data={
        'sort':'start_real',
        'dir':'ASC'
    }
)

delete = []
for rec in r.json()['entries'] :
    if rec['filename'].startswith(args.records_dir) and rec['uuid'] not in exclude :
        df = shutil.disk_usage(rec['filename']).free
        print ("Espace libre : %s"%human_size(df))
        if df <= df_req:
            rep = None
            if not args.yes_all :
                while rep not in ['', 'y','yes','all','n','no','e','exit'] :
                    rep = input("Supprimer \"%s %s\" (uuid %s) ? (YES, all, no, exit)\n> "%(rec['disp_title'], rec['disp_subtitle'], rec['uuid'])).lower()
                if rep in ['a', 'all'] :
                    args.yes_all = True
                elif rep in ['e', 'exit'] :
                    sys.exit(0)
            else :
                print ("Effacement \"%s %s\""%(rec['disp_title'], rec['disp_subtitle']))
            if rep in ['', 'y', 'yes'] or args.yes_all :
                os.remove(rec['filename'])
        else :
            break
