#!/usr/bin/env python3
# -*- coding: utf8 -*-

#  Copyright (c) 2021
#  Jonas Fourquier (http://jonas.cloud) based on tr4ck3ur work
#  https://mythtv-fr.org/forums/viewtopic.php?pid=25265#p25265
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import sys, os, requests, argparse, time, logging
import xml.etree.ElementTree as ET
from datetime import datetime
from pytz import timezone
from slugify import slugify

TZ = timezone('Europe/Paris')
B_URL = "http://mafreebox.freebox.fr"
BASE_URL = "http://mafreebox.freebox.fr/api/v3/"
CHANNEL_URL = BASE_URL + "tv/channels/"
ICON_URL = BASE_URL + "tv/img/channels/logos68x60/"
EPG_URL = BASE_URL + "tv/epg/by_channel/"
PROG_URL = BASE_URL + "tv/epg/programs/"

CONFIG_FILE = os.path.join(os.environ['HOME'],'.xmltv','tv_grab_fr_mafreebox.conf')

parser = argparse.ArgumentParser(description = "Grabber xmltv via l'api de la freebox")
parser.add_argument("--description", action = 'store_true', help="Description")
parser.add_argument("--capabilities", action = 'store_true', help="Capactitée")
parser.add_argument("--configure", action = 'store_true', help="Configurer le grabber")
parser.add_argument("--config-file", default=CONFIG_FILE, help="Fichier de configuration (%s par défaut)"%CONFIG_FILE)
parser.add_argument("--quiet", action = 'store_true', help="Supprime toutes les sorties de progressions")
parser.add_argument("--verbose", action = 'store_true', help="Bavard (affiche plus d'information de progression)")
parser.add_argument("--debug", action = 'store_true', help="Affiche des informations pour le debug")
parser.add_argument("--days", type=int, default=10, help="Nombre de jour (approximatif) à télécharger. 10 par défaut")
args = parser.parse_args()

if args.debug :
    logLevel = logging.DEBUG
elif args.verbose :
    logLevel = logging.INFO
elif args.quiet :
    logLevel = logging.CRITICAL
else :
    logLevel = logging.WARNING

logging.basicConfig(
    format = "%(asctime)s %(levelname)s - %(message)s",
    level = logLevel
  )

categories_en = {
# Selon https://tvheadend.org/projects/tvheadend/repository/entry/src/epg.c
    1:  "Movie / Drama", # Film
    2:  "Movie / Drama", # Téléfilm
    3:  "Movie / Drama", # Série /Feuilleton
    4:  "Movie / Drama", # Feuilleton
    5:  "Documentary", # Documentaire
    6:  "Performing arts", #Théatre
    7:  "Opera", #Opéra
    8:  "Ballet", #Ballet
    9:  "Variety show", # Variétés
    10: "Magazines / Reports / Documentary", # Magazine
    11: "Children's / Youth programmes", # Jeunesse
    12: "Game show / Quiz / Contes", # Jeu
    13: "Music / Ballet / Dance", # Musique
    14: "Variety show", # Divertissement
    16: "Cartoons / Puppets", # Dessins animés
    19: "Sports", # Sport
    20: "News / Current affairs", # Journal
    22: "Discussion / Interview / Debate", # Débats
    24: "Performing arts", # Spectacle
    31: "Religion" # Emission religieuse
}  #todo à compléter


def get_cfg_chan() :
    '''
        Recupérère la liste des chaines configurées dans le fichier de
        configuration.
    '''
    if not os.path.isfile(args.config_file) :
        logging.error("Non configuré, lancez %s --configure"%__file__)
        sys.exit(1)
    list_chan = []
    with open(args.config_file) as fp:
        for line in fp :
            chan_name = line.split('#',1)[0].strip()
            if chan_name :
                list_chan.append(slugify(chan_name))
    logging.debug("Liste des chaînes configurée :\n\t%s"%"\n\t".join(list_chan))
    return list_chan


def get_hts_chan():
    '''
        Recupérère la liste des chaines configurée sur hts.
    '''
    url = (input ("URL de hts (\"http://localhost:9981/\" par défaut) : ") or "http://localhost:9981/")
    user = (input ("Utilisateur hts (\"admin\" par défaut) : ") or "admin")
    pswd = (input ("Mot de passe(\"admin\" par défaut) : ") or "admin")
    list_chan = []
    reponse = requests.get('%s/api/channel/list'%url,auth=(user,pswd))
    if not reponse.ok :
        logging.error ("Erreur de connection au serveur hts (erreur http code %i)"%reponse.status_code)
        sys.exit(1)
    for entrie in reponse.json()['entries'] :
        list_chan.append(slugify(entrie['val']))
    return list_chan


def get_mythtv_chan():
    '''
        Recupérère la liste des chaines configurée sur mythtv.
    '''
    list_chan = []
    db = MythDB()
    SchemaVersion = db.settings.NULL.DBSchemaVer
    channels = Channel.getAllEntries()
    for chan in channels:
        if chan.sourceid==3:
            list_chan.append(slugify.slugify(chan.name))
    return list_chan


def do_request(my_url):
    '''
        Execute une requète http.
    '''
    logging.debug(my_url)
    time.sleep(1)
    response = requests.get(my_url)
    out_dic = response.json()
    while "msg" in out_dic and out_dic["msg"]=="Too many requests":
        logging.info("Oups : trop de requètes, nouvelle tentative dans 60''")
        time.sleep(60)
        logging.info("Nouvelle tentative ..")
        response = requests.get(my_url)
        out_dic = response.json()
    if "success" in out_dic and out_dic["success"]==False:
        logging.error("Oups : %s"%response.text)
    return out_dic.get('result',{})


def start_time():
    '''
        L'heure actuelle.
    '''
    now=int(time.time())
    return now-now%7200


def format_time(dt):
    '''
        Format de l'heure selon spécification xmltv
    '''
    return datetime.strftime(
        datetime.fromtimestamp(dt, tz=TZ),
        "%Y%m%d%H%M%S %z")


def get_epg_data(root, channel_uid, channel_slug, start_time):
    '''
        Complète l'xmltv <root> pour la chaine <channel_uid> à partir de
        <start_time>
    '''
    # Scan EPG to get program data
    epg_progs = {}
    nb_slots = args.days * 12
    for time_slot in range(nb_slots):
        epgurl = EPG_URL + "%s/%s/" % (channel_uid, start_time+time_slot*7200)
        dic_epg = do_request(epgurl)
        logging.info("Récupération de %d programes (il y en avait %d avant cette requête)"%(len(dic_epg), len(epg_progs)))
        # don't handle fake results
        if len(dic_epg)==1:
            for k,v in dic_epg.items():
                if 'fake' in v:
                    logging.debug("\"Fake result\", stop la récupération des données epg")
                    return False
        epg_progs.update(dic_epg)

    for key, obj in epg_progs.items():
        cur_prog = ET.Element('programme')
        cur_prog.attrib['start'] = format_time( obj.get('date'))
        cur_prog.attrib['stop'] = format_time( obj.get('date') + obj.get('duration') )
        cur_prog.attrib['channel'] = channel_slug
        # Add Title
        if 'title' in obj:
            title = ET.Element('title')
            title.text = obj.get('title')
            cur_prog.append(title)
        # Add Sub-Title
        if 'sub_title' in obj:
            subtitle = ET.Element('sub-title')
            subtitle.text = obj.get('sub_title')
            cur_prog.append(subtitle)
        # Add Description
        # desc = ET.Element('desc')
        # desc.text = str( obj.get('desc') )
        # check if need description depending category
        if 'category' in obj and obj.get('category') not in [0, 5, 6, 9, 10, 12, 13, 14, 19, 20, 22, 31]:
            # Ok let's check if we have a description
            epg_id = obj.get('id')
            prog_url = PROG_URL + "%s" % (epg_id)
            dic_prog = do_request(prog_url)
            if 'desc' in dic_prog:
                desc = ET.Element('desc')
                desc.attrib['lang'] = "fr"
                desc.text = dic_prog.get('desc')
                cur_prog.append(desc)
            else:
                logging.debug("Pas de description trouvée")
        # Add category
        if 'category_name' in obj:
            if obj.get('category') in categories_en :
                #traduction de la catégorie en anglais pour le filtre de hts
                category_en = ET.Element('category')
                category_en.attrib['lang'] = "en"
                category_en.text = categories_en[obj.get('category')]
                cur_prog.append(category_en)
            else :
                logging.warning("Traduction de la catégorie inconnue merci de rapporter le problème %d:\"%s\""%(obj.get('category'), obj.get('category_name')))
            category_fr = ET.Element('category')
            category_fr.attrib['lang'] = "fr"
            category_fr.text = obj.get('category_name')
            cur_prog.append(category_fr)
        # Add length
        if 'duration' in obj:
            length = ET.Element('length')
            length.attrib['units'] = "seconds"
            length.text = str(obj.get('duration'))
        cur_prog.append(length)
        # Add icon
        if 'picture_big' in obj:
            icon = ET.Element('icon')
            icon.attrib['src'] = B_URL+obj.get('picture_big')
            cur_prog.append(icon)
        elif 'picture' in obj:
            icon = ET.Element('icon')
            icon.attrib['src'] = B_URL+obj.get('picture')
            cur_prog.append(icon)
       	# Add episode-num
        if 'episode_number' in obj and 'season_number' in obj:
            episode = ET.Element('episode-num')
            episode.attrib['system'] = "xmltv_ns"
            episode.text="%d . %d . 0" % (obj.get('season_number')-1,
                                    obj.get('episode_number')-1)
            cur_prog.append(episode)
        root.append(cur_prog)
    return True


def build_xml():
    '''
        Construit l'xmltv
    '''
    root = ET.Element('tv')
    root.set('generator-info-name', 'snouf XMLTV generated')
    root.set('generator-info-url', 'https://gitlab.com/snouf/tvheadend_tools')

    dic_channels = do_request(CHANNEL_URL)
    logging.info("%d chaines trouvée"%len(dic_channels))
    cfg_chan = get_cfg_chan()
    start = start_time()
    i=0
    for chan_name, channel in dic_channels.items():
        if slugify(channel.get('name')) not in cfg_chan:
            logging.info("%s pas dans la liste, saut"%channel.get('name'))
            continue
        channel_slug = slugify(channel.get('name')).lower()+'.mafreebox.fr'
        cur_channel = ET.Element('channel')
        cur_channel.attrib['id'] = channel_slug
        icon_channel = ET.Element('icon')
        icon_channel.attrib['src'] = B_URL + channel.get('logo_url')
        disp_channel = ET.Element('display-name')
        disp_channel.text = channel.get('name')
        cur_channel.append(disp_channel)
        cur_channel.append(icon_channel)
        logging.debug("Récupération epg pour %s"%channel.get('name'))
        root.insert(i, cur_channel)
        i += 1
        get_epg_data(root,
                    channel.get('uuid'),
                    channel_slug,
                    start_time())

    return ET.tostring(root)


def configure () :
    '''
        Lance la configuration
    '''
    choise = input(\
        "Configuration de la liste des chaines pour lequel récupérer l'epg :\n"+\
        "  1 : Toutes par défaut\n"+\
        "  2 : Aucune par défaut\n"+\
        "  3 : Préconfiguration à partir de hts tvheadend\n"+\
        "  4 : Préconfiguration à partir de mythtv (beta, non testé)\n"+\
        "Vous pouvez à tout moment éditeriter la liste des chaines dans le fichier %s\n"\
        %args.config_file
      )
    if choise not in ('1','2','3') :
        logging.error("Entrée invalide")
        sys.exit(1)

    dic_channels = do_request(CHANNEL_URL)
    channels_names = [chan.get('name') for chan in dic_channels.values()]
    channels_names.sort() #ordre alphanumérique plus pratique pour trouver une chaine
    list_chan = []
    if choise == '3' :
        hts_chan = get_hts_chan()
    elif choise == '4' :
        mythtv_chan= get_mythtv_chan()

    for chan_name in channels_names:
        if choise == '1'\
         or (choise == '3' and slugify(chan_name) in hts_chan)\
         or (choise == '4' and slugify(chan_name) in mythtv_chan) :
            list_chan.append("%s # %s"%(chan_name,slugify(chan_name).lower()+".mafreebox.fr"))
        else :
            list_chan.append("#%s # %s"%(chan_name,slugify(chan_name).lower()+".mafreebox.fr"))

    if not os.path.isdir(os.path.dirname(args.config_file)) :
        os.makedirs(os.path.dirname(args.config_file))
    with open(args.config_file, 'w') as fp:
        fp.write(\
            "# Liste des chaines récupére dans le guide\n"+\
            "# Les lignes qui commencent par un # sont ignorées\n\n"+\
            "\n".join(list_chan)
        )


if __name__ == '__main__':
    if args.description :
        sys.stdout.write("France (mafreebox.fr)\n")
    elif args.capabilities :
        sys.stdout.write("manualconfig\nbaseline\n")
    elif args.configure :
        configure()
    else :
        sys.stdout.write(build_xml().decode())
    sys.exit(0)

